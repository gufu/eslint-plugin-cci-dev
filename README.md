# eslint-plugin-cci-dev

Collection of eslint rules for cci-dev frontend projects

## Rules

|Rule ID|Description|
|---|---|
|[cci-dev/vue-html-require-data-automation-id](/docs/rules/vue-html-require-data-automation-id.md)|enforces existence of data-automation-id attribute in vue components|


## Setup
Eslint setup in your project: 

<code>package.json</code>
```json
{
  "devDependencies": {
    "eslint-plugin-cci-dev": "1.0.0"
  } 
}
```

<code>.eslintrc.json</code>
```json
{
  "plugins": [
    "cci-dev"
  ],
  "rules": {
    "cci-dev/[rule-name]":  ["error", "config"]
  }
}
```