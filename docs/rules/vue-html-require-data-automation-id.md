# Clickable elements require data-automation-id attribute

For automatic test purposes clickable elements of frontend projects
require some kind of attribute to identify elements on the page. This
attribute is specified as <code>data-automation-id</code> with value of
unique name/identifier for an element.


## Rule Details

This rule enforces existence of data-automation-id attribute in vue
components. By default it checks html tags: 
```js
[
    "a",
    "b-alert",
    "b-button",
    "b-card",
    "b-carousel",
    "b-carousel-slide",
    "b-collapse",
    "b-dropdown",
    "b-dropdown-item",
    "b-form",
    "b-form-checkbox",
    "b-form-file",
    "b-form-input",
    "b-form-radio",
    "b-form-select",
    "b-form-textarea",
    "b-link",
    "b-modal",
    "b-nav-item",
    "b-popover",
    "b-tab",
    "b-tabs",
    "b-tooltip",
    "button",
    "details",
    "dialog",
    "fieldset",
    "form",
    "input",
    "menu",
    "menuitem",
    "optgroup",
    "option",
    "select",
    "summary",
    "textarea"
]
```

Rule checks if one of event types exists on component: 
```vue
@click      // v-on:click
@hover      // v-on:hover
```

Rule checks if one of directives exists on component: 
```vue
b-popover   // https://bootstrap-vue.js.org/docs/components/popover (BootstrapVue popover)
```

## Setup
Eslint setup in your project: 

<code>package.json</code>
```json
{
  "devDependencies": {
    "eslint-plugin-cci-dev": "1.0.0"
  } 
}
```

<code>.eslintrc.json</code>
```json
{
  "plugins": [
    "cci-dev"
  ],
  "rules": {
    "cci-dev/vue-html-require-data-automation-id":  ["error", {
      "ignore": ["ignore-tag"],
      "include": ["include-tag"]
    }]
  }
}
```

## Options

- <code>"ignore": ["string"]</code> - ignore tags
- <code>"include": ["string"]</code> - add additional tags to be checked

### ignore
:thumbsdown: Examples of <b>incorrect</b> code for this rule with <code>[ {
"ignore": ["a"] } ]</code> options:
```vue
/*eslint cci-dev/vue-html-require-data-automation-id: [ { "ignore": ["a"] } ] */
<button id="test"></button>
<input name="testInput" id="testInput" />
```

:thumbsup: Examples of <b>correct</b> code for this rule with <code>[ {
"ignore": ["a"] } ]</code> options:
```vue
/*eslint cci-dev/vue-html-require-data-automation-id: [ { "ignore": ["a"] } ] */
<a href="#"></a>
<input name="testInput" id="testInput" data-automation-id="testInput" />
```

### include
:thumbsdown: Examples of <b>incorrect</b> code for this rule with
<code>[ { "include": ["span"] } ]</code> options:
```vue
/*eslint cci-dev/vue-html-require-data-automation-id: [ { "include": ["span"] } ] */
<span>test</span>
<input name="testInput" id="testInput" />
```

:thumbsup: Examples of <b>correct</b> code for this rule with <code>[ {
"include": ["span"] } ]</code> options:
```vue
/*eslint cci-dev/vue-html-require-data-automation-id: [ { "include": ["span"] } ] */
<span data-automation-id="test">test</span>
<input name="testInput" id="testInput" data-automation-id="testInput" />
```

## When Not To Use It

You can turn this rule off if you are not concerned with automatic tests
for your components.

## Related Rules

None

## Version

Introduced in eslint-plugin-cci-dev 1.0.0

## Resources
- [Rule source](../../lib/rules/vue-html-require-data-automation-id.js)
- [Rule tests](../../tests/lib/rules/vue-html-require-data-automation-id.js)
