/**
 * @fileoverview Clickable elements should have a data-automation-id attribute
 * @author Konrad Gusz
 */
'use strict'
const utils = require('../utils')
const casing = require('../utils/casing')

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

module.exports = {
  meta: {
    type: 'suggestion',
    docs: {
      description: 'enforce data-automation-id attribute on interactive elements in vue components',
      category: 'strongly-recommended',
      url: 'https://.../'
    },
    schema: [
      {
        type: 'object',
        properties: {
          ignore: {
            type: 'array',
            items: { type: 'string' },
            uniqueItems: true,
            additionalItems: false
          },
          include: {
            type: 'array',
            items: {
              allOf: [
                {type: 'string'},
                { not: { type: 'string', pattern: ':exit$' }},
                { not: { type: 'string', pattern: '^\\s*$' }}
              ]
            },
            uniqueItems: true,
            additionalItems: false
          }
        },
        additionalProperties: false
      }
    ],
    messages: {
      tagRequiresAttribute: 'Tag ("<{{name}}>") requires data-automation-id attribute, but none was found.'
    }
  },

  create (context) {
    const options = context.options[0]
    const optionsInclude = options && options['include'] ? options['include'] : []
    const optionsIgnore = options && options['ignore'] ? options['ignore']: []

    const htmlNodes = ['a', 'button', 'fieldset', 'form', 'input', 'optgroup', 'option', 'select', 'textarea', 'details', 'dialog', 'menu', 'menuitem', 'summary']
    const vueNodes = ['b-alert', 'b-button', 'b-card', 'b-carousel', 'b-carousel-slide', 'b-collapse', 'b-dropdown', 'b-dropdown-item', 'b-form', 'b-form-input', 'b-form-select',
      'b-form-checkbox', 'b-form-file', 'b-form-radio', 'b-form-textarea', 'b-link', 'b-modal', 'b-nav-item', 'b-popover', 'b-tabs', 'b-tab', 'b-tooltip']
    const eventTypes = ['click', 'hover']
    const directives = ['b-popover']

    const allNodes = [...htmlNodes, ...vueNodes, ...optionsInclude]
    const filteredNodes = allNodes.filter(x => !optionsIgnore.includes(x))

    function hasAutomationAttribute (node) {
      return node.attributes.some(x => {
        return x.key.name === 'data-automation-id' && (x.value && x.value.value !== '')
      })
    }

    function hasEventAttribute (node) {
      return node.attributes.some(x => {
        return x.directive && x.key.name === 'on' && eventTypes.indexOf(x.key.argument) !== -1
      })
    }

    function hasDirective (node) {
      return node.attributes.some(x => {
        return x.directive && directives.indexOf(x.key.raw.name) !== -1
      })
    }

    function shouldHaveAutomationAttribute (node) {
      return filteredNodes.indexOf(node.name) !== -1 || hasDirective(node.startTag) || hasEventAttribute(node.startTag)
    }

    function reportIssue (node, name) {
      context.report({
        node,
        messageId: 'tagRequiresAttribute',
        data: {
          name: node.rawName
        }
      })
    }

    // ----------------------------------------------------------------------
    // Public
    // ----------------------------------------------------------------------

    return utils.defineTemplateBodyVisitor(context, {
      VElement (node) {
        const shouldHaveAttribute = shouldHaveAutomationAttribute(node)
        if(shouldHaveAttribute) {
          const name = node.rawName
          const hasAttribute = hasAutomationAttribute(node.startTag)
          if(!hasAttribute) {
            reportIssue(node, name)
          }
        }
      }
    })
  }
}

