/**
 * @fileoverview Clickable elements should have a data-automation-id attribute
 * @author Konrad Gusz
 */
'use strict'

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

const rule = require('../../../lib/rules/vue-html-require-data-automation-id')
const RuleTester = require('eslint').RuleTester

const tester = new RuleTester({
  parser: require.resolve('vue-eslint-parser'),
  parserOptions: {
    ecmaVersion: 2017
  }
})

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------

tester.run('vue-html-require-data-automation-id', rule, {

  valid: [
    // give me some code that won't trigger a warning
    {
      code: '<template><a href="" data-automation-id="link-name">test</a></template>'
    },
    {
      code: '<template><whatever data-automation-id="link-name">test</whatever></template>',
      options: [{
        'include': ['whatever']
      }]
    },
    {
      code: '<template><a>test</a></template>',
      options: [{
        'ignore': ['a']
      }]
    }
  ],

  invalid: [
    {
      code: '<template><a href="">test</a></template>',
      output: '<template><a href="">test</a></template>',
      errors: [
        {
          messageId: 'tagRequiresAttribute'
        }
      ]
    },
    {
      code: '<template><whatever>test</whatever></template>',
      output: '<template><whatever>test</whatever></template>',
      options: [{
        'include': ['whatever']
      }],
      errors: [
        {
          messageId: 'tagRequiresAttribute'
        }
      ]
    }
  ]
})
